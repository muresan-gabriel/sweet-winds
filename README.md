# Sweet Winds

# Presentation of workflow

## 30.03.2022

Created the default react application, deleted unnecessary files.

## 31.03.2022

 Research for React routing and implementation.

 Divided elements into components and created the basic layout for the Signup / Login pages.

 Somewhere during this day I realised last time I started from scratch a React application, I was using versions 16 and 17. Updated my knowledge and rendered the 'App' in the newly created root.

 Imported a custom web-font, Montserrat.

 Scrapped some of the components and imported them in a single .js document.

## 01.04.2022

 Created a basic table with manually added values to test out displaying Google Maps on the page.

 Attempted fetch of login data and working on the login page functionalities.

 Added some custom styling in addition to using Bootstrap for layouts, components and responsiveness.
 
 Added media queries to display the forms accordingly on wider viewports.
 Confused about certain aspects, I started following a React course to polish out knowledge and abilities in a different repository on GitHub and afterwards I added everything from the GitHub repository to the GitLab repository. Certain details, such as the 'name' field in .json files were imported aswell, getting overwritten to 'react-course' from 'sweet-winds'. Fixed later.

## 02.04.2022

 Numerous attempts at implementing a maps API (tried Google, Leaflet, but didn't attempt implementation of MapBox). The documentation that allowed me to implement the API with a minimum number of errors is [this one](https://developers.google.com/maps/documentation/javascript/react-map), but unfortunately when rendering the Dashboard page, the whole page is blank and nothing is rendered on the root, not even the table nor the navbar. Must be fixed.
 Attempted to implement a new login function that uses a token aswell and commented out Map components to avoid errors until it's fixed.
 After a few tests, I tried again to implement a map API by using code from a pre-made prototype, thinking I am doing something wrong. Was unsuccessful in this aswell, couldn't tell what I was doing wrong.

## 03.04.2022

 Abandoned login and maps implementation and tried to continue with other components of the application. Was successful in fetching spot data and displaying it into the table.
 Worked on responsiveness on the table.
 Fixed title from past course repository from 'react-course' to 'sweet-winds' in .json files.



Last update: 03.04.2022, 13:06

Access to the repository has been provided around this time. Project will probably be updated again around 23:00 in the course of this day.
This might also be the final version.

Despite attempting to learn React a few years ago, my JS abilities were not good enough, progress was not made in the React learning process.
This project forced me to learn React sooner than planned, thinking my present JS knowledge was still not enough for React. I was wrong and I was able to learn a lot while working on this project. I also learnt to fetch data from APIs, what RESTful APIs are and how to use this data, details unknown to me previously.

So far, this was the greatest technical 'interview' I was assigned. I am looking forward to feedback, fixes or a working version of this application to be able to compare.

Thank you in advance!
