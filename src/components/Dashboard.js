import { Route, Link } from "react-router-dom";
import React, { useRef, useEffect, useState } from "react";
import Navbar from "./Navbar.js";
import MapWrapper from "./MapWrapper.js";
import axios from "axios";

function Dashboard() {
  const [spots, setSpots] = useState([]);
  useEffect(() => {
    fetchSpots();
  }, []);
  const fetchSpots = () => {
    axios
      .get("https://62467974739ac845918f04c5.mockapi.io/spot")
      .then((res) => {
        console.log(res);
        setSpots(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  return (
    <div>
      <Navbar></Navbar>

      {/* MUST FIX MAP IMPLEMENTATION */}

      {/* <MapWrapper /> */}

      {/* MUST FIX MAP IMPLEMENTATION */}

      <div className="container-fluid row">
        <div className="col-md"></div>
        <div className="spots-table-container col-md">
          <table className="table-responsive spots-table container">
            <thead>
              <tr>
                <th scope="col">ID</th>
                <th scope="col">Name</th>
                <th scope="col">Country</th>
                <th scope="col">Latitude</th>
                <th scope="col">Longitude</th>
                <th scope="col">Wind Prob.</th>
                <th scope="col">When to go</th>
              </tr>
            </thead>
            <tbody>
              {spots.map((spot) => (
                <tr className="spots-data">
                  <td>{spot.id}</td>
                  <td>{spot.name}</td>
                  <td>{spot.country}</td>
                  <td>{spot.lat}</td>
                  <td>{spot.long}</td>
                  <td>{spot.probability}</td>
                  <td>{spot.month}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}

export default Dashboard;
