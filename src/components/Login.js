import { Route, Link } from "react-router-dom";
import { useEffect, useState } from "react";
import Navbar from "./Navbar.js";
import PropTypes from "prop-types";

async function loginUser(credentials) {
  return fetch("https://62467974739ac845918f04c5.mockapi.io/login", {
    method: "POST",
    mode: "no-cors",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(credentials),
  }).then((data) => data.json());
}

function Login({ setToken }) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();
    const token = await loginUser({
      email,
      password,
    });
    setToken(token);
  };

  return (
    <div>
      <Navbar></Navbar>
      <div className="loginContainer container">
        <h4 className="mb-5">Welcome to Sweet Winds!</h4>
        <h4 className="mb-3">Login</h4>
        <form onSubmit={handleSubmit}>
          <div className="mb-3">
            <input
              type="email"
              className="form-control"
              id="loginEmail"
              aria-describedby="emailHelp"
              placeholder="Email"
              onChange={(e) => setEmail(e.target.value)}
            />
          </div>
          <div className="mb-3">
            <input
              type="password"
              className="form-control"
              id="loginPassword"
              placeholder="Password"
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>
          <p>Don't have an account? {/*<Link to="/signup">Sign up</Link>*/}</p>
          <button type="submit" className="btn btn-primary">
            Login
          </button>
        </form>
      </div>
    </div>
  );
}

export default Login;

Login.propTypes = {
  setToken: PropTypes.func.isRequired,
};
